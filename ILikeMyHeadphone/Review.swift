//
//  Review.swift
//  ILikeMyHeadphone
//
//  Created by Edgard Matos on 10/07/15.
//  Copyright (c) 2015 Deway. All rights reserved.
//

import UIKit

class Review: NSObject {
    var id:Int?
    var user:Int?
    var brand:String?
    var model:String?
    var rating:Int?
    var comment:String?
    var photo:String?
}
