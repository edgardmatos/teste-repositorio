//
//  LoginViewController.swift
//  ILikeMyHeadphone
//
//  Created by Edgard Matos on 09/07/15.
//  Copyright (c) 2015 Deway. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.navigationBarHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func login(sender: AnyObject) {
        if (!isValidEmail(self.emailField.text)) {
            let alert = UIAlertView()
            alert.title = "Erro!"
            alert.message = "Digite um e-mail válido."
            alert.addButtonWithTitle("OK")
            alert.show()
            return
        }
        
        if (count(self.passwordField.text) < 6) {
            let alert = UIAlertView()
            alert.title = "Erro!"
            alert.message = "Sua senha deve conter no mínimo 6 caracteres."
            alert.addButtonWithTitle("OK")
            alert.show()
            return
        }
        
        let alert = UIAlertView()
        alert.title = "OK"
        alert.message = "Formulário Válido!"
        alert.addButtonWithTitle("OK")
        alert.show()
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    func isValidEmail(testStr:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
